#!/usr/bin/env node
/**
 * @author Joris Gutjahr
 * @license MIT
 * This is a PoC for sharing files through
 * IPFS by a human friendly name.
 * This means, that the only thing to be
 * shared between receiver and sender of a file prior
 * to the transfer of said file
 * is a user given string.
 */
const IPFS = require("ipfs")
const yargs = require("yargs")
const fs = require("fs")
const IPFS_REPO = process.env.IPFS_REPO || "~/.jsipfs"

async function main() {
  const ipfs = await IPFS.create({ silent: true, repo: IPFS_REPO })

  yargs.scriptName("ipfs-share")
       .usage("$0 <cmd> [args]")
       .command("share [file] [name]", "Share a file under name", yargs => {
         yargs.positional("name", {
           type: "string",
           describe: "The name to share as"
         })
         yargs.positional("file", {
           type: "string",
           describe: "The file to share"
         })
       }, async argv => {
         const name = argv.name
         const file = argv.file

         fs.readFile(file, async (error, data) => {
           if(error) throw error

           if(typeof(data) === typeof("")) {
             data = Buffer.from(data, "utf-8")
           }

           data = new Uint8Array(data);


           const result = await ipfs.add(data)
           const cid = result.cid.toString()
           console.error("Publishing CID " + cid)

           ipfs.pubsub.publish(name, cid)
         })
       })
       .command("receive [name]", "Receive a file shared under name", yargs => {
         yargs.positional("name", {
           type: "string",
           describe: "Name to receive file from"
         })
       }, async argv => {
         const name = argv.name

         ipfs.pubsub.subscribe(name, async message => {
           const cid = message.data.toString("utf-8")

           console.error("Receiving CID " + cid)

           for await (const file of ipfs.get(cid)) {
             const content = []

             for await (const chunk of file.content) {
               content.push(chunk)
             }

             console.log(content.toString())
             process.exit()
           }
         })
       })
       .recommendCommands()
       .help()
       .argv
}

main()
